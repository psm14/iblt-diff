use iblt::*;
use iblt::hasher::*;
use std::fmt;

static HASH_MAGIC_SEED: u32 = 1337;

#[derive(Serialize, Deserialize)]
pub struct HashBucket {
  count: isize,
  hash: u32,
  check: u32
}

impl HashBucket {
  fn valid_checksum(&self) -> bool {
    let check = self.hash.hash(HASH_MAGIC_SEED);
    check == self.check
  }
}

impl IBLTNodeFromNothing for HashBucket {
  fn new() -> Self {
    HashBucket {
      count: 0,
      hash: 0,
      check: 0
    }
  }
}

impl fmt::Display for HashBucket {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let checksum = self.hash.hash(HASH_MAGIC_SEED);
    let checksum = if self.check == checksum {
      format!("{}", self.check)
    } else {
      format!("{} != {}", self.check, checksum)
    };
    write!(f, "N: {} C: {} V: {}", self.count, checksum, self.hash)
  }
}

impl IBLTNode for HashBucket {
  fn is_empty(&self) -> bool {
    self.count == 0 && self.hash == 0
  }

  fn count(&self) -> isize {
    self.count
  }

  fn has_valid_single_item(&self) -> bool {
    (self.count == 1 || self.count == -1) && self.valid_checksum()
  }
}

impl InsertNode for HashBucket {
  type I = u32;

  fn insert(&mut self, item: &u32, add: bool) -> bool {
    self.hash ^= *item;
    self.check ^= self.hash.hash(HASH_MAGIC_SEED);
    self.count += if add { 1 } else { -1 };
    return true;
  }

  fn insert_all(&mut self, bucket: &Self, add: bool) -> bool {
    self.hash ^= bucket.hash;
    self.check ^= bucket.check;
    if add {
      self.count += bucket.count;
    } else {
      self.count -= bucket.count;
    }
    return true;
  }
}

impl ScanNode<u32> for HashBucket {
  fn scan(&self, func: &mut FnMut(bool, &u32)) -> bool {
    if self.is_empty() {
      return false;
    } if self.has_valid_single_item() {
      let diff = self.count > 0;
      func(diff, &self.hash);
    }
    true
  }
}
