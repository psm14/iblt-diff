use iblt::*;
use std::fmt;
use std::cmp;
use iblt::hasher::*;

static HASH_MAGIC_SEED: u32 = 1337;

type Checksum = (u64, u64);

#[derive(Serialize, Deserialize)]
pub struct VecBucket {
  count: isize,
  bytes: Vec<u8>,
  size: usize,
  checksum: Checksum
}

impl VecBucket {
  fn write_size(&mut self, size: usize) {
    self.size ^= size;
  }

  fn write_checksum(&mut self, checksum: Checksum) {
    self.checksum.0 ^= checksum.0;
    self.checksum.1 ^= checksum.1;
  }

  fn content_checksum(&self) -> Checksum {
    let len = cmp::min(self.size, self.bytes.len());
    let bytes = &self.bytes[0..len];
    bytes.hash(HASH_MAGIC_SEED)
  }

  fn mix(&mut self, item: &[u8]) {
    for (x, y) in self.bytes.iter_mut().zip(item) {
      *x ^= *y;
    }
  }
}

impl fmt::Display for VecBucket {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let checksum = self.content_checksum();
    let checksum = if checksum == self.checksum {
      format!("({},{})", self.checksum.0, self.checksum.1)
    } else {
      format!("({},{}) != ({},{})", self.checksum.0, self.checksum.1, checksum.0, checksum.1)
    };
    write!(f, "N: {} S: {} C: {}", self.count, self.size, checksum)
  }
}

impl IBLTNodeFromParams for VecBucket {
  type P = usize;
  fn from_params(size: usize) -> Self {
    VecBucket {
      count: 0,
      bytes: vec![0; size],
      size: 0,
      checksum: (0,0)
    }
  }
}

impl IBLTNode for VecBucket {
  fn is_empty(&self) -> bool {
    self.count == 0 && self.checksum == (0,0) && self.size == 0
  }

  fn count(&self) -> isize {
    self.count
  }

  fn has_valid_single_item(&self) -> bool {
    if !(self.count == 1 || self.count == -1) {
      return false;
    }
    if self.size > self.bytes.len() {
      return false;
    }
    self.checksum == self.content_checksum()
  }
}

impl InsertNode for VecBucket {
  type I = [u8];

  fn insert(&mut self, item: &[u8], add: bool) -> bool {
    let len = item.len();
    if len > self.bytes.len() {
      return false;
    }

    let checksum = item.hash(HASH_MAGIC_SEED);
    self.write_checksum(checksum);
    self.write_size(len);
    self.mix(item);
    self.count += if add { 1 } else { -1 };
    return true;
  }

  fn insert_all(&mut self, bucket: &Self, add: bool) -> bool {
    if bucket.bytes.len() != self.bytes.len() {
      return false;
    }

    self.write_checksum(bucket.checksum);
    self.write_size(bucket.size);
    self.mix(&bucket.bytes);
    if add {
      self.count += bucket.count;
    } else {
      self.count -= bucket.count;
    }
    return true;
  }
}

impl ScanNode<[u8]> for VecBucket {
  fn scan(&self, func: &mut FnMut(bool, &[u8])) -> bool {
    if self.is_empty() {
      return false;
    } else if self.has_valid_single_item() {
      let diff = self.count > 0;
      func(diff, &self.bytes[0..self.size]);
    }
    true
  }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn create_empty() {
        let bucket = VecBucket::from_params(1);
        assert_eq!(bucket.count, 0);
        assert_eq!(bucket.bytes.len(), 1);
        assert_eq!(bucket.bytes[0], 0);
        assert_eq!(bucket.has_valid_single_item(), false);
    }

    #[test]
    fn add_one() {
        let mut bucket = VecBucket::from_params(1);
        bucket.insert(&[42], true);
        assert_eq!(bucket.count, 1);
        assert_eq!(bucket.bytes.len(), 1);
        assert_eq!(bucket.bytes[0], 42);
        assert_eq!(bucket.has_valid_single_item(), true);
        assert_eq!(bucket.is_empty(), false);
    }

    #[test]
    fn remove_one() {
        let mut bucket = VecBucket::from_params(1);
        bucket.insert(&[42], false);
        assert_eq!(bucket.count, -1);
        assert_eq!(bucket.bytes.len(), 1);
        assert_eq!(bucket.bytes[0], 42);
        assert_eq!(bucket.has_valid_single_item(), true);
        assert_eq!(bucket.is_empty(), false);
    }

    #[test]
    fn add_two() {
        let mut bucket = VecBucket::from_params(1);
        bucket.insert(&[42], true);
        bucket.insert(&[66], true);
        assert_eq!(bucket.count, 2);
        assert_eq!(bucket.bytes.len(), 1);
        assert_eq!(bucket.has_valid_single_item(), false);
        assert_eq!(bucket.is_empty(), false);
    }

    #[test]
    fn add_remove_different() {
        let mut bucket = VecBucket::from_params(1);
        bucket.insert(&[42], true);
        bucket.insert(&[66], false);
        assert_eq!(bucket.count, 0);
        assert_eq!(bucket.bytes.len(), 1);
        assert_eq!(bucket.has_valid_single_item(), false);
        assert_eq!(bucket.is_empty(), false);
    }

    #[test]
    fn add_remove() {
        let mut bucket = VecBucket::from_params(1);
        bucket.insert(&[42], true);
        bucket.insert(&[66], true);
        bucket.insert(&[42], false);
        assert_eq!(bucket.count, 1);
        assert_eq!(bucket.bytes.len(), 1);
        assert_eq!(bucket.bytes[0], 66);
        assert_eq!(bucket.has_valid_single_item(), true);
    }
     
    #[test]
    fn combine() {
        let mut bucket_a = VecBucket::from_params(1);
        bucket_a.insert(&[42], true);
        bucket_a.insert(&[66], true);

        let mut bucket_b = VecBucket::from_params(1);
        bucket_b.insert(&[42], true);

        bucket_a.insert_all(&bucket_b, false);
        assert_eq!(bucket_a.count, 1);
        assert_eq!(bucket_a.bytes.len(), 1);
        assert_eq!(bucket_a.bytes[0], 66);
        assert_eq!(bucket_a.has_valid_single_item(), true);
    }
}