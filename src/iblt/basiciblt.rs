use super::*;
use iblt::hasher::*;
use std::fmt;

#[derive(Serialize, Deserialize)]
pub struct BasicIBLT<B: IBLTNode> {
  seed: u32,
  buckets: Vec<B>
}

impl <B: IBLTNode + fmt::Display> DebugIBLT for BasicIBLT<B> {
  fn debug_buckets(&self) {
    for bucket in self.buckets.iter() {
      if !bucket.is_empty() {
        eprintln!("Non-empty bucket: {}", bucket);
      }
    }
  }
}

impl <B: IBLTNode + IBLTNodeFromParams> IBLTNodeFromParamsAndIndex for BasicIBLT<B> {
  type P = (usize, B::P);

  fn from_params_and_index((num_buckets, bucket_params,): Self::P, seed: u32) -> Self {
    let mut buckets = Vec::with_capacity(num_buckets);
    for _ in 0..num_buckets {
      let bucket = B::from_params(bucket_params);
      buckets.push(bucket);
    }
    BasicIBLT {
      seed,
      buckets
    }
  }
}

impl <B: IBLTNode> ListNode for BasicIBLT<B> {
  type B = B;

  fn buckets(&self) -> &Vec<B> {
    &self.buckets
  }
}

impl <I: ?Sized + Hashable<u32>, B: IBLTNode + InsertNode<I = I>> InsertNode for BasicIBLT<B> {
  type I = I;

  fn insert(&mut self, value: &Self::I, do_add: bool) -> bool { 
    let hash = value.hash(self.seed);
    let target_bucket = hash as usize % self.buckets.len();
    let bucket = &mut self.buckets[target_bucket];
    bucket.insert(value, do_add)
  }

  fn insert_all(&mut self, other: &Self, do_add: bool) -> bool {
    if other.buckets.len() != self.buckets.len() {
      return false;
    }
    for i in 0..self.buckets.len() {
      let mut bucket_a = &mut self.buckets[i];
      let bucket_b = &other.buckets[i];
      let result = bucket_a.insert_all(bucket_b, do_add);
      if !result {
        return false;
      }
    }
    return true;
  }
}
