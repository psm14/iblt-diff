pub mod bucket;
pub mod hasher;
pub mod basiciblt;
pub mod stratumiblt;
pub mod multihashiblt;

pub trait IBLTNode {
  fn is_empty(&self) -> bool;
  fn has_valid_single_item(&self) -> bool;
  fn count(&self) -> isize;
}

pub trait ListNode {
  type B: IBLTNode;
  fn buckets(&self) -> &Vec<Self::B>;
}

impl <T: ListNode> IBLTNode for T {
  fn is_empty(&self) -> bool {
    for bucket in self.buckets().iter() {
      if !bucket.is_empty() {
        return false;
      }
    }
    true
  }

  fn count(&self) -> isize {
    let mut count = 0;
    for bucket in self.buckets().iter() {
      count += bucket.count();
    }
    count
  }

  fn has_valid_single_item(&self) -> bool {
    let count = self.count();
    if !(count == 1 || count == -1) {
      return false;
    }
    for bucket in self.buckets().iter() {
      if !bucket.is_empty() {
        return bucket.has_valid_single_item();
      }
    }
    return false;
  } 
}

impl <I: ?Sized, B: IBLTNode + ScanNode<I>, T: ListNode<B = B>> ScanNode<I> for T {
  fn scan(&self, func: &mut FnMut(bool, &I)) -> bool {
    let mut any_not_empty = false;
    for bucket in self.buckets().iter() {
      let not_empty = bucket.scan(func);
      any_not_empty = any_not_empty || not_empty;
    }
    any_not_empty
  }
}

pub trait InsertNode {
  type I: ?Sized;

  fn insert(&mut self, value: &Self::I, add: bool) -> bool;
  fn insert_all(&mut self, other: &Self, add: bool) -> bool;
}

pub trait DebugIBLT {
  fn debug_buckets(&self);
}

pub trait ScanNode<I: ?Sized> {
  fn scan(&self, func: &mut FnMut(bool, &I)) -> bool;
}

pub trait ConsumeIBLT<I: ?Sized> {
  fn consume(&mut self, func: &mut FnMut(bool, &I)) -> bool;
}

pub trait IBLTNodeFromParams {
  type P: Copy;
  fn from_params(params: Self::P) -> Self;
}

pub trait IBLTNodeFromParamsAndIndex {
  type P: Copy;
  fn from_params_and_index(params: Self::P, index: u32) -> Self;
}

impl <T: IBLTNodeFromParams> IBLTNodeFromParamsAndIndex for T {
  type P = T::P;
  fn from_params_and_index(params: Self::P, _: u32) -> Self {
    T::from_params(params)
  }
}

pub trait IBLTNodeFromNothing {
  fn new() -> Self;
}

impl <T: IBLTNodeFromNothing> IBLTNodeFromParams for T {
  type P = ();
  fn from_params(_: ()) -> Self {
    T::new()
  }
}

pub trait StratumEstimator {
  fn stratum(&mut self) -> usize;
}
