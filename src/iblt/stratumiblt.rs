use super::*;
use iblt::hasher::*;
use iblt::basiciblt::BasicIBLT;
use iblt::multihashiblt::MultiHashIBLT;
use iblt::bucket::hashbucket::HashBucket;
use std::cmp;

#[derive(Serialize, Deserialize)]
pub struct StratumIBLT {
  buckets: Vec<MultiHashIBLT<BasicIBLT<HashBucket>>>
}

impl IBLTNodeFromParams for StratumIBLT {
  type P = usize;

  fn from_params(bucket_size: usize) -> Self {
    let mut buckets = Vec::with_capacity(32);
    for _ in 0..32 {
      let bucket = MultiHashIBLT::from_params((1, (bucket_size, ())));
      buckets.push(bucket);
    }

    StratumIBLT {
      buckets: buckets
    }
  }
}

impl ListNode for StratumIBLT {
  type B = MultiHashIBLT<BasicIBLT<HashBucket>>;
  fn buckets(&self) -> &Vec<Self::B> {
    &self.buckets
  }
}

impl InsertNode for StratumIBLT {
  type I = [u8];

  fn insert(&mut self, value: &Self::I, do_add: bool) -> bool { 
    let hash: u32 = value.hash(0);
    let zero_bits = hash.trailing_zeros();
    let bucket = &mut self.buckets[zero_bits as usize];
    bucket.insert(&hash, do_add)
  }

  fn insert_all(&mut self, other: &Self, do_add: bool) -> bool {
    if other.buckets.len() != self.buckets.len() {
      return false;
    }
    for i in 0..self.buckets.len() {
      let mut bucket_a = &mut self.buckets[i];
      let bucket_b = &other.buckets[i];
      let result = bucket_a.insert_all(bucket_b, do_add);
      if !result {
        return false;
      }
    }
    return true;
  }
}

impl StratumEstimator for StratumIBLT {
  fn stratum(&mut self) -> usize {
    let mut successful_count = 0;
    let mut lowest_successful = 32;
    for i in 0..32 {
      let bucket = &mut self.buckets[31 - i];
      let mut count = 0;
      let success = bucket.consume(&mut |_,_| { count += 1 });
      if success {
        lowest_successful = 32 - i;
        successful_count += count;
      }
    }
    let magnitude = 2_usize.pow(lowest_successful as u32);
    return magnitude * cmp::max(successful_count, 1);
  }
}
