use super::*;
use iblt::hasher::*;

#[derive(Serialize, Deserialize)]
pub struct MultiHashIBLT<B: IBLTNode> {
  buckets: Vec<B>
}

impl <T: IBLTNode + DebugIBLT> DebugIBLT for MultiHashIBLT<T> {
  fn debug_buckets(&self) {
    for bucket in self.buckets.iter() {
      bucket.debug_buckets();
    }
  }
}

impl <B: IBLTNode + IBLTNodeFromParamsAndIndex> IBLTNodeFromParams for MultiHashIBLT<B> {
  type P = (usize, B::P);
  fn from_params((hashes, child_params,): Self::P) -> Self {
    let mut buckets = Vec::with_capacity(hashes);
    for i in 0..hashes {
      let bucket = B::from_params_and_index(child_params, i as u32);
      buckets.push(bucket);
    }
    MultiHashIBLT {
      buckets
    }
  }
}

impl <B: IBLTNode> ListNode for MultiHashIBLT<B> {
  type B = B;

  fn buckets(&self) -> &Vec<B> {
    &self.buckets
  }
}

impl <I: ?Sized + Hashable<u32>, B: IBLTNode + InsertNode<I = I>> InsertNode for MultiHashIBLT<B> {
  type I = I;

  fn insert(&mut self, value: &Self::I, do_add: bool) -> bool { 
    for bucket in self.buckets.iter_mut() {
      let result = bucket.insert(value, do_add);
      if !result {
        return false;
      }
    }
    true
  }

  fn insert_all(&mut self, other: &Self, do_add: bool) -> bool {
    if self.buckets.len() != other.buckets.len() {
      return false;
    }
    for (a, b) in self.buckets.iter_mut().zip(other.buckets.iter()) {
      let result = a.insert_all(&b, do_add);
      if !result {
        return false;
      }
    }
    true
  }
}

pub trait CopyResult {
  type C;
  fn copy(&self) -> Self::C;
  fn to_ref(item: &Self::C) -> &Self;
}

impl CopyResult for [u8] {
  type C = Vec<u8>;
  fn copy(&self) -> Vec<u8> {
    self.to_vec()
  }
  fn to_ref(item: &Vec<u8>) -> &[u8] {
    &item
  }
}

impl CopyResult for u32 {
  type C = u32;
  fn copy(&self) -> u32 {
    *self
  }
  fn to_ref(item: &u32) -> &u32 {
    item
  }
}

impl <T: ?Sized + CopyResult + Hashable<u32>, B: IBLTNode + InsertNode<I = T> + ScanNode<T>> ConsumeIBLT<T> for MultiHashIBLT<B> {
  fn consume(&mut self, func: &mut FnMut(bool, &T)) -> bool {
    loop {
      let mut any_found = false;
      let mut all_empty = true;
      for i in 0..self.buckets.len() {
        let mut results: Vec<(bool, T::C)> = Vec::new();
        {
          let b = &self.buckets[i];
          let not_empty = b.scan(&mut |diff, item| {
            func(diff, &item);
            any_found = true;
            results.push((diff, item.copy()))
          });
          all_empty = all_empty && !not_empty;
        }
        for (diff, item) in results {
          self.insert(T::to_ref(&item), !diff);
        }
      }
      if !any_found {
        return all_empty;
      }
    }
  }
}
