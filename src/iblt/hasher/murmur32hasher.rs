use super::Hashable;
use murmurhash3;
use std::mem;
use std::slice;

impl Hashable<u32> for [u8] {
    fn hash(&self, index: u32) -> u32 {
        murmurhash3::murmurhash3_x86_32(self, index)
    }
}

impl Hashable<u32> for [usize] {
    fn hash(&self, index: u32) -> u32 {
        let scale = mem::size_of::<usize>() / mem::size_of::<u8>();
        let bytes: *const [usize] = self;
        let bytes: *const u8 = bytes as *const _;
        let bytes: &[u8] = unsafe {
            slice::from_raw_parts(bytes, self.len() * scale)
        };
        murmurhash3::murmurhash3_x86_32(bytes, index)
    }
}

impl Hashable<u32> for u32 {
    fn hash(&self, index: u32) -> u32 {
        let num: *const u32 = self;
        let num: *const u8 = num as *const _;
        let num_bytes: &[u8] = unsafe {
            slice::from_raw_parts(num, mem::size_of::<u32>())
        };
        murmurhash3::murmurhash3_x86_32(num_bytes, index)
    }
}
