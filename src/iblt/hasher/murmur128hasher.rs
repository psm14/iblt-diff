use super::Hashable;
use murmurhash3;
use std::mem;
use std::slice;

impl Hashable<(u64, u64)> for [u8] {
    fn hash(&self, index: u32) -> (u64, u64) {
        murmurhash3::murmurhash3_x64_128(self, index as u64)
    }
}

impl Hashable<(u64, u64)> for [usize] {
    fn hash(&self, index: u32) -> (u64, u64) {
        let scale = mem::size_of::<usize>() / mem::size_of::<u8>();
        let bytes: *const [usize] = self;
        let bytes: *const u8 = bytes as *const _;
        let bytes: &[u8] = unsafe {
            slice::from_raw_parts(bytes, self.len() * scale)
        };
        murmurhash3::murmurhash3_x64_128(bytes, index as u64)
    }
}
