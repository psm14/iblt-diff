mod murmur32hasher;
mod murmur128hasher;

pub use self::murmur32hasher::*;
pub use self::murmur128hasher::*;

pub trait Hashable<O> {
    fn hash(&self, index: u32) -> O;
}
