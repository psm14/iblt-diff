extern crate ibltlib;
use ibltlib::*;
use ibltlib::iblt::*;
use ibltlib::iblt::basiciblt::*;
use ibltlib::iblt::multihashiblt::*;
use ibltlib::iblt::bucket::vecbucket::VecBucket;
use ibltlib::iblt::StratumEstimator;
use ibltlib::iblt::stratumiblt::StratumIBLT;

use std::io::BufReader;
use std::io::BufWriter;
use std::io::BufRead;
use std::io::prelude::*;
use std::fs::File;
use std::fs;
use std::str;
use std::cmp;
use std::process;

extern crate clap;
use clap::{Arg, App};

extern crate bincode;
extern crate serde;

trait ErrorExit<A> {
    fn unwrap_or_exit(self, message: &str) -> A;
}

impl <A,B: std::fmt::Debug> ErrorExit<A> for Result<A,B> {
    fn unwrap_or_exit(self, message: &str) -> A {
        if self.is_err() {
            eprintln!("Error: {}", message);
            process::exit(1);
        }
        self.unwrap()
    }
}

impl <A> ErrorExit<A> for Option<A> {
    fn unwrap_or_exit(self, message: &str) -> A {
        if self.is_none() {
            eprintln!("Error: {}", message);
            process::exit(1);
        }
        self.unwrap()
    }
}

fn create_output(output: Option<&str>, plus_output: Option<&str>) -> Box<DiffOutput> {
    match output {
        None => Box::new(ConsoleDiffOutput {}),
        Some(f) => {
            let file = File::create(f).unwrap_or_exit("Couldn't create output file");
            let writer = BufWriter::new(file);
            match plus_output {
                None => Box::new(FileOutput { writer, path: f.to_string() }),
                Some(p) => {
                    let file = File::create(p).unwrap_or_exit("Couldn't create output file");
                    let plus_writer = BufWriter::new(file);
                    let output = DualFileOutput {
                        minus_writer: writer,
                        minus_path: f.to_string(),
                        plus_writer: plus_writer,
                        plus_path: p.to_string()
                    };
                    Box::new(output)
                }
            }
        }
    }
}

fn process_file<I: InsertNode<I = [u8]>>(iblt: &mut I, file: &str, bucket_size: usize, left_offset: usize, right_offset: usize, add: bool, verbosity: u8) -> usize {
    if verbosity > 0 {
        eprintln!("Processing file: {}", file);
    }

    let f = File::open(file).unwrap_or_exit("Couldn't open file");
    let file = BufReader::new(&f);

    let mut longest_line = 0;
    let mut i = 0;
    for line in file.lines() {
        let l = line.unwrap_or_exit("Couldn't read file");
        if l.len() > left_offset {
            let slice = &l.as_bytes()[left_offset..(l.len()-right_offset)];
            let end = cmp::min(slice.len(), bucket_size);
            if end > longest_line {
                longest_line = end;
            }
            let success = iblt.insert(&slice[..end], add);
            if !success {
                eprintln!("Error constructing IBLT");
                process::exit(1);
            }
        }
        if verbosity > 1 {
            i = i + 1;
            if i % 100000 == 0 {
                eprintln!("Processed {} records", i);
            }
        }
    };
    longest_line 
}

fn read_iblt(path: &str) -> MultiHashIBLT<BasicIBLT<VecBucket>> {
    let f = File::open(path).unwrap_or_exit("Couldn't open file");
    let r = BufReader::new(&f);
    bincode::deserialize_from(r).unwrap_or_exit("Couldn't deserialize IBLT")
}

fn process_diff<'a, I: ConsumeIBLT<[u8]>>(iblt: &'a mut I, output: &mut Box<DiffOutput>, verbosity: u8) -> bool {
    let mut added = 0;
    let mut removed = 0;
    let success = iblt.consume(&mut |diff, item| {
        output.output(diff, item);
        if diff {
            added += 1;
        } else {
            removed += 1;
        }
        if verbosity > 1 && (added + removed) % 10000 == 0 {
            eprintln!("Decoded {} additions and {} removals", added, removed);
        }
    });
    if verbosity > 0 {
        eprintln!("Decoded {} additions and {} removals", added, removed);
    }
    if !success {
        output.cancel();
    }
    success
}

trait DiffOutput {
    fn output(&mut self, diff: bool, line: &[u8]);
    fn cancel(&mut self);
}

struct ConsoleDiffOutput {}
impl DiffOutput for ConsoleDiffOutput {
    fn output(&mut self, diff: bool, line: &[u8]) {
        let diff = if diff { '+' } else { '-' };
        println!("{}{}", diff, String::from_utf8(line.to_vec()).unwrap_or_exit("Couldn't write output"));
    }

    fn cancel(&mut self) {}
}

struct FileOutput {
    writer: BufWriter<File>,
    path: String
}
impl DiffOutput for FileOutput {
    fn output(&mut self, diff: bool, line: &[u8]) {
        let diff = if diff { '+' as u8 } else { '-' as u8 };
        self.writer.write_all(&[diff]).unwrap_or_exit("Couldn't write output");
        self.writer.write_all(line).unwrap_or_exit("Couldn't write output");
        self.writer.write_all(&['\n' as u8]).unwrap_or_exit("Couldn't write output");
    }

    fn cancel(&mut self) {
        fs::remove_file(&self.path).unwrap_or_exit("Couldn't delete partial result file");
    }
}

struct DualFileOutput {
    minus_writer: BufWriter<File>,
    minus_path: String,
    plus_writer: BufWriter<File>,
    plus_path: String
}
impl DiffOutput for DualFileOutput {
    fn output(&mut self, diff: bool, line: &[u8]) {
        let writer = if diff { &mut self.plus_writer } else { &mut self.minus_writer };
        writer.write_all(line).unwrap_or_exit("Couldn't write output");
        writer.write_all(&['\n' as u8]).unwrap_or_exit("Couldn't write output");
    }

    fn cancel(&mut self) {
        fs::remove_file(&self.minus_path).unwrap_or_exit("Couldn't delete partial result file");
        fs::remove_file(&self.plus_path).unwrap_or_exit("Couldn't delete partial result file");
    }
}

fn main() {
    let matches = App::new("IBLT")
        .version("1.0")
        .author("Patrick M. <me@patmclaughl.in>")
        .about("Computes and diffs files and IBLTs")
        .arg(Arg::with_name("verbose")
            .short("v")
            .long("verbose")
            .multiple(true)
            .help("Verbosity level"))
        .arg(Arg::with_name("INPUT_1")
            .help("First input file")
            .required(true)
            .index(1))
        .arg(Arg::with_name("INPUT_2")
            .help("Second input file")
            .required(false)
            .index(2))
        .arg(Arg::with_name("output")
            .short("o")
            .long("output")
            .value_name("FILE")
            .help("Output diff file")
            .required(false)
            .takes_value(true))
        .arg(Arg::with_name("plus-output")
            .short("p")
            .long("plus-output")
            .value_name("FILE")
            .help("If specified, output '+' diffs here and '-' diffs to the other output file"))
        .arg(Arg::with_name("buckets")
            .short("b")
            .long("buckets")
            .value_name("NUM")
            .help("Number of buckets (Max if difference estimation is used)")
            .takes_value(true))
        .arg(Arg::with_name("bucket-size")
            .short("s")
            .long("bucket-size")
            .value_name("BYTES")
            .help("Bucket size in bytes (Max if difference estimation is used)")
            .takes_value(true))
        .arg(Arg::with_name("estimator-buckets")
            .short("u")
            .long("estimator-buckets")
            .value_name("NUM")
            .help("Number of buckets to use for set difference estimation (default 100)")
            .takes_value(true))
        .arg(Arg::with_name("fail-over-limits")
            .short("f")
            .long("fail-over-limits")
            .help("Fail if estimated sizes are over specified --buckets and --bucket-size values"))
        .arg(Arg::with_name("num-hashes")
            .short("h")
            .long("num-hashes")
            .value_name("NUM")
            .help("Number of hashes (default 4)")
            .takes_value(true))
        .arg(Arg::with_name("safety-factor")
            .short("a")
            .long("safety-factor")
            .value_name("NUM")
            .help("Amount to multiply the estimated set difference by (default 2)")
            .takes_value(true))
        .arg(Arg::with_name("offset-left")
            .short("l")
            .long("offset-left")
            .value_name("BYTES")
            .help("Skip this many bytes at the start of a line")
            .takes_value(true))
        .arg(Arg::with_name("offset-right")
            .short("r")
            .long("offset-right")
            .value_name("BYTES")
            .help("Skip this many bytes at the end of a line")
            .takes_value(true))
        .arg(Arg::with_name("estimate-only")
            .short("e")
            .long("estimate-only")
            .help("Only run set difference estimation, don't diff files"))
        .arg(Arg::with_name("no-retries")
            .short("y")
            .long("no-retries")
            .help("Don't retry if first IBLT fails to decode"))
        .arg(Arg::with_name("no-estimate")
            .short("z")
            .long("no-estimate")
            .help("Don't estimate set difference before starting and use specified values"))
        .arg(Arg::with_name("create-iblt")
            .short("c")
            .long("create-iblt")
            .help("Create and output an IBLT instead of a diff"))
        .arg(Arg::with_name("diff-iblts")
            .short("d")
            .long("diff-iblts")
            .help("Read input files as IBLTs"))
        .get_matches();

    let prev = matches.value_of("INPUT_1").unwrap_or_exit("No input file specified");
    let output = matches.value_of("output");
    let plus_output = matches.value_of("plus-output");
    let verbosity = cmp::min(matches.occurrences_of("verbose"), 2) as u8;
    let buckets = matches.value_of("buckets");
    let bucket_size = matches.value_of("bucket-size");
    let num_hashes = matches.value_of("num-hashes").unwrap_or("4");
    let num_hashes: usize = num_hashes.parse().unwrap_or_exit("--num-hashes value was not a number");
    let left_offset = matches.value_of("offset-left").unwrap_or("0");
    let left_offset: usize = left_offset.parse().unwrap_or_exit("--offset-left value was not a number");
    let right_offset = matches.value_of("offset-right").unwrap_or("0");
    let right_offset: usize = right_offset.parse().unwrap_or_exit("--offset-right value was not a number");
    let dont_estimate = matches.occurrences_of("no-estimate") > 0;
    let estimate_only = matches.occurrences_of("estimate-only") > 0;
    let create_iblt = matches.occurrences_of("create-iblt") > 0;
    let diff_iblts = matches.occurrences_of("diff-iblts") > 0;
    let safety_factor: f32 = matches.value_of("safety-factor").unwrap_or("2").parse().unwrap_or_exit("--safety-factor value was not a number");
    let estimator_buckets = matches.value_of("estimator-buckets").unwrap_or("100").parse().unwrap_or_exit("--estimator-buckets value was not a number");
    let fail_over_limits = matches.occurrences_of("fail-over-limits") > 0;
    let no_retries = matches.occurrences_of("no-retries") > 0;
    let current = matches.value_of("INPUT_2");

    if estimate_only && dont_estimate {
        eprintln!("Conflicting options --estimate-only and --dont-estimate specified");
        process::exit(1);
    } else if fail_over_limits && dont_estimate {
        eprintln!("Conflicting options --fail-over-limits and --dont-estimate specified");
        process::exit(1);
    } else if create_iblt && current.is_some() {
        eprintln!("Multiple files specified with --create-iblt");
        process::exit(1);
    } else if create_iblt && output.is_none() {
        eprintln!("Please specify an output file");
        process::exit(1);
    } else if diff_iblts && estimate_only {
        eprintln!("--diff-iblts is not compatible with --estimate-only");
        process::exit(1);
    }

    if create_iblt {
       let buckets = buckets.unwrap_or_exit("--buckets not specified").parse().unwrap_or_exit("--buckets value was not a number");
       let bucket_size = bucket_size.unwrap_or_exit("--bucket-size not specified").parse().unwrap_or_exit("--bucket-size value was not a number"); 
       let mut iblt = default_iblt(buckets, num_hashes, bucket_size);
       process_file(&mut iblt, &prev, bucket_size, left_offset, right_offset, true, verbosity);
       let mut file = File::create(output.unwrap_or_exit("--output not specified")).unwrap_or_exit("Couldn't create output file");
       let w = BufWriter::new(&mut file);
       bincode::serialize_into(w, &iblt).unwrap_or_exit("Couldn't write output file");
    } else {
        let mut output_writer: Box<DiffOutput> = create_output(output, plus_output);
        let current = current.unwrap_or_exit("Second input file not specified");

        let success = if diff_iblts {
            let mut iblt_a = read_iblt(prev);
            let iblt_b = read_iblt(current);
            let success = iblt_a.insert_all(&iblt_b, false);
            if !success {
                eprintln!("Couldn't diff IBLTs. Check that they are the same size");
                process::exit(1);
            }
            process_diff(&mut iblt_a, &mut output_writer, verbosity)
        } else {
            let (buckets, max_buckets, bucket_size) = if !dont_estimate {
                if verbosity > 0 {
                    eprintln!("Estimating set difference with {} buckets", estimator_buckets);
                }
                let mut stratum = StratumIBLT::from_params(estimator_buckets);
                let mut longest_line = 0;
                let len = process_file(&mut stratum, &prev, usize::max_value(), left_offset, right_offset, false, verbosity);
                if len > longest_line {
                    longest_line = len;
                }
                let len = process_file(&mut stratum, &current, usize::max_value(), left_offset, right_offset, true, verbosity);
                if len > longest_line {
                    longest_line = len;
                }
                let estimate = stratum.stratum();
                let scaled_buckets = (estimate as f32 * safety_factor) as usize;
                let max_buckets = buckets.map(|b| { b.parse().unwrap_or_exit("--buckets value was not a number") }).unwrap_or(scaled_buckets * 8);
                if fail_over_limits && scaled_buckets > max_buckets {
                    eprintln!("Estimated number of buckets {} over limit of {}", scaled_buckets, max_buckets);
                    process::exit(1);
                }
                let buckets = cmp::min(scaled_buckets, max_buckets);

                let max_size = bucket_size.map(|b| { b.parse().unwrap_or_exit("--bucket-size value was not a number") }).unwrap_or(usize::max_value());
                if fail_over_limits && longest_line > max_size {
                    eprintln!("Longest line length {} over limit of {}", longest_line, max_size);
                    process::exit(1);
                }
                let bucket_size = cmp::min(longest_line, max_size);

                if estimate_only {
                    eprintln!("Estimated difference: {}", estimate);
                    eprintln!("Longest line seen: {} bytes", longest_line);
                    process::exit(0);
                }

                if verbosity > 0 {
                    eprintln!("Using {} buckets of size {} (max {})", buckets, bucket_size, max_buckets);
                }
                (buckets, max_buckets, bucket_size)
            } else {
                let buckets = buckets.unwrap_or_exit("--buckets not specified").parse().unwrap_or_exit("--buckets value was not a number");
                let bucket_size = bucket_size.unwrap_or_exit("--bucket-size not specified").parse().unwrap_or_exit("--bucket-size value was not a number");
                (buckets, buckets, bucket_size)
            };

            let mut success;
            let mut buckets = buckets;
            let mut output_writer = output_writer;
            loop {
                let mut iblt = default_iblt(buckets, num_hashes, bucket_size);
                process_file(&mut iblt, &prev, bucket_size, left_offset, right_offset, false, verbosity);
                process_file(&mut iblt, &current, bucket_size, left_offset, right_offset, true, verbosity);
                success = process_diff(&mut iblt, &mut output_writer, verbosity);
                if success || no_retries || buckets >= max_buckets {
                    break;
                } else {
                    buckets = cmp::min(buckets * 2, max_buckets);
                    output_writer = create_output(output, plus_output);
                    if verbosity > 0 {
                        eprintln!("IBLT failed to decode, retrying with {} buckets", buckets);
                    }
                }
            }
            success
        };

        if !success {
            if verbosity > 0 {
                eprintln!("Couldn't fully decode IBLT.");
            }
            process::exit(1);
        } else if verbosity > 0 {
            eprintln!("Successfully decoded IBLT");
        }
    }
}
