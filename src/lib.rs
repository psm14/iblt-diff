#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate murmurhash3;

pub mod iblt;
use iblt::*;

use iblt::basiciblt::BasicIBLT;
use iblt::multihashiblt::MultiHashIBLT;
use iblt::bucket::vecbucket::VecBucket;

pub fn default_iblt(approx_buckets: usize, num_hashes: usize, bucket_size: usize) -> MultiHashIBLT<BasicIBLT<VecBucket>> {
  let num_buckets = if approx_buckets % num_hashes == 0 { approx_buckets / num_hashes } else { (approx_buckets / num_hashes) + 1 };
  MultiHashIBLT::from_params((num_hashes, (num_buckets, bucket_size)))
}
